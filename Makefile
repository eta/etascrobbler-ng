.PHONY: all

all:
	CC=musl-gcc go build -ldflags='-extldflags=-static'
