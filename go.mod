module etascrobbler-ng

go 1.17

require (
	crawshaw.io/sqlite v0.3.2
	golang.org/x/crypto v0.0.0-20220427172511-eb4f295cb31f
)

require (
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/text v0.3.6 // indirect
)
